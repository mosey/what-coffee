'use strict';
var app = angular.module('grinder', [
	'ngRoute',
	'grinderPeople',
]);

// Main routing connections
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	//Front end links
	$routeProvider
		.when('/', {
			templateUrl: 'views/templates/users/user-listings.html',
			controller: 'peopleController'
	})
	.when('/user/:name', {
		templateUrl: 'views/templates/users/individual.html',
		controller: 'individualController'
	})

	// 404 page
	.otherwise({
		templateUrl: '/views/templates/404.html',
  });

  // $locationProvider.html5Mode(true).hashPrefix('!');;
}])
