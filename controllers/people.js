var app = angular.module('grinderPeople', []);

// Landing page controller
app.controller('peopleController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
	if($routeParams.newUser) $scope.newUser = $routeParams.newUser;
	if($routeParams.deleted) {
 		$scope.deleted = true;
 		$scope.deleteName = $routeParams.deleteName;
	}

	var people = $scope;
	people.persons = [];
	$scope.hiddenPeople = false;
	$scope.individual = {};

	// Gets the people
	$http.get('./data/staff.json').success(function(data) {
		people.persons = data;
	});

	// Hides the people and gets the information for the individual
	this.hiderFunction = function(name) {
		$scope.hiddenPeople = true;
		people.persons.forEach(function(el, i) {
			if(el.name == name) {
				$scope.individual = el;
				$scope.coffee = el.beverages.coffee;
				$scope.tea = el.beverages.tea;
				return;
			}
		})
	};

	// Resets our search
	this.resetHider = function() {
		$scope.searchText = '';
		if($scope.hiddenPeople) $scope.hiddenPeople = !$scope.hiddenPeople;
	};
}])

// Person controller
app.controller('individualController', ['$scope', '$http', '$routeParams', '$location', function ($scope, $http, $routeParams, $location) {
	var userName = $routeParams.name;
	if($routeParams.updated) {
		$scope.updated = true;
	}
	$scope.individual = {};
	$scope.coffeeShake = false;
	$scope.teaShake = false;

	// Gets the users details
	$http.get('./data/staff.json').success(function(data) {
		angular.forEach(data, function(val, key) {
			if(val.name == userName) {
				$scope.individual = val;
				$scope.coffee = val.beverages.coffee;
				$scope.tea = val.beverages.tea;
				$scope.sugarPrefAmount = new Array(parseInt($scope.coffee.sugarAmount));
			}
		})

	});

}])

app.directive('coffeeDrink', function() {
 return {
	 restrict: 'E',
	 templateUrl: './views/templates/drinks/coffee-drink.html',
 }
})

app.directive('teaDrink', function() {
 return {
	 restrict: 'E',
	 templateUrl: './views/templates/drinks/tea-drink.html'
 }
})
